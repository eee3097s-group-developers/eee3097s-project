#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#define EI 11  /* typically 10..13 */
#define EJ  4  /* typically 4..5 */
#define P   1  /* If match length <= P then output one character */
#define N (1 << EI)  /* buffer size */
#define F ((1 << EJ) + 1)  /* lookahead buffer size */

int bit_buffer = 0, bit_mask = 128;
unsigned long codecount = 0, textcount = 0;
unsigned char buffer[N * 2];
char compressedText[100];
char input[] =  "28.080601,-0.121951,0.243902,-0.182927,-0.000488,0.003906,1.001953\n"
                "28.320215,0.060976,-0.243902,-0.365854,0.001953,-0.006836,1.010254\n"
                "28.320215,0.304878,-0.365854,0.060976,-0.005371,0.000488,1.006836\n"
                "28.320215,-0.060976,0.060976,-0.548781,-0.003906,0.000000,1.009766\n"
                "28.224369,0.243902,-0.304878,-0.243902,0.000000,0.001465,1.007324\n";
int main(void){
    encode(input);
}

void error(void){
	exit(1);
}

void putbit1(void)
{
	bit_buffer |= bit_mask;
	if ((bit_mask >>= 1) == 0) {
		compressedText[codecount] = bit_buffer;
		// need to make sure compressedText array is not full before it gets here.
		// Solve this in while(1) by encoding every couple recordings
		bit_buffer = 0;  bit_mask = 128;  codecount++;
	}
}

void putbit0(void)
{
	if ((bit_mask >>= 1) == 0) {
		compressedText[codecount] = bit_buffer;
		bit_buffer = 0;  bit_mask = 128;  codecount++;
	}
}

void flush_bit_buffer(void)
{
	if (bit_mask != 128) {
		compressedText[codecount] = bit_buffer;
		codecount++;
	}
}

void output1(int c)
{
	int mask;

	putbit1();
	mask = 256;
	while (mask >>= 1) {
		if (c & mask) putbit1();
		else putbit0();
	}
}

void output2(int x, int y)
{
	int mask;

	putbit0();
	mask = N;
	while (mask >>= 1) {
		if (x & mask) putbit1();
		else putbit0();
	}
	mask = (1 << EJ);
	while (mask >>= 1) {
		if (y & mask) putbit1();
		else putbit0();
	}
}

void encode(char inputData[])
{
	// create compressedText char array here and initialise as null. Send through array to other functions.
    int i, j, f1, x, y, r, s, bufferend, c;
    for (i = 0; i < N - F; i++) buffer[i] = ' ';
    for (i = N - F; i < N * 2; i++) {
        if (textcount > strlen(inputData)-1) break;
        buffer[i] = inputData[textcount++];
    }
    bufferend = i;  r = N - F;  s = 0;
    while (r < bufferend) {
        f1 = (F <= bufferend - r) ? F : bufferend - r;
        x = 0;  y = 1;  c = buffer[r];
        for (i = r - 1; i >= s; i--)
            if (buffer[i] == c) {
                for (j = 1; j < f1; j++)
                    if (buffer[i + j] != buffer[r + j]) break;
                if (j > y) {
                    x = i;  y = j;
                }
            }
        if (y <= P) {  y = 1;  output1(c);  }
        else output2(x & (N - 1), y - 2);
        r += y;  s += y;
        if (r >= N * 2 - F) {
            for (i = 0; i < N; i++) buffer[i] = buffer[i + N];
            bufferend -= N;  r -= N;  s -= N;
            while (bufferend < N * 2) {
                if (textcount > strlen(inputData)-1) break;
                buffer[bufferend++] = inputData[textcount++];

            }
        }
    }
    flush_bit_buffer();
}