%This MATLAB code was used to perform the Fourier Analysis on the inital recorded data
filename = 'WalkingAroundDataMAG.csv';
X = csvread(filename);

accelX = X(1:34328,22);

y = fft(accelX);
Ts = 1/2;
fs = 1/Ts;
f = (0:length(y)-1)*fs/length(y);
n = length(accelX);
fshift = (-n/2:n/2-1)*(fs/n);
yshift = fftshift(y);
plot(fshift, abs(yshift));
xlabel("Frequency (Hz)");
ylabel("Magnitude");
xlim([-0.5 0.5]);
