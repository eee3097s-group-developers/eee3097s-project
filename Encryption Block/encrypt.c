#include "stdlib.h"
#include "stdio.h"
#include <string.h>
#include <time.h>

int main(void){
    FILE *input_file = fopen("SensorData.csv","rb");
    FILE *output_file = fopen("SensorData_Encrypted.csv", "wb");
    int key = 50;

    char ch;
    clock_t t;
    t = clock();
    while((ch = fgetc(input_file))!= EOF){
        //putchar(ch);
        ch = ch + key;
        fputc(ch, output_file);
    }
    t = clock()-t;
    fclose(input_file);
    fclose(output_file);

    //double time_taken = ((double)t)/CLOCKS_PER_SEC;
    //printf("Encryption time: %lf\n",time_taken);
    
return 0;
}