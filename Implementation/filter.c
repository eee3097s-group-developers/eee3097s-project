#include "stdlib.h"
#include "stdio.h"
#include <string.h>

int main(void){
    FILE *input_file = fopen("OutputUnfiltered.csv","rb");
    FILE *output_file = fopen("OutputFiltered.csv", "a");
    int countComma = 0;
    char ch;
    int count = 0;
    while((ch = fgetc(input_file))!= EOF){
            if (ch == ','){countComma++;}
            if (countComma == 6){count++;}
            if (count + countComma > 15 ){break;}

            fputc(ch, output_file);
        }
    fputc('\n', output_file);
    fclose(input_file);
    fclose(output_file);
    return 0;
}