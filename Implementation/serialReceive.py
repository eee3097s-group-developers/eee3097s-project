import serial, subprocess
serialPort = serial.Serial(port = "COM5", baudrate=9600, bytesize=8, timeout=2, stopbits=serial.STOPBITS_ONE)

serialString = ""                           # Used to hold data coming over UART
i = 0

while(1):
    # Wait until there is data waiting in the serial buffer
    if(serialPort.in_waiting > 0):
        # Read in 72 bytes of data    
        serialString = serialPort.read(72)

        with open("EncryptedCompressed.csv", "wb") as file:
            file.write(serialString)
        file.close()

        tmp=subprocess.call("./decompressDecrypt.exe")
        subprocess.call("./filter.exe")

        print(tmp)

        i += 1
        print(i)
        serialPort.reset_input_buffer()


        
