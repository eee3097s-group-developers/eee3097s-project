/* LZSS encoder-decoder (Haruhiko Okumura; public domain) */
// Used for overall functionality (decompress method called)
// decompresses from a file

#include <stdio.h>
#include <stdlib.h>
#include <time.h> // ADDED for clock

#define EI 11  /* typically 10..13 */
#define EJ  4  /* typically 4..5 */
#define P   1  /* If match length <= P then output one character */
#define N (1 << EI)  /* buffer size */
#define F ((1 << EJ) + 1)  /* lookahead buffer size */

int key = 50;
int bit_buffer = 0, bit_mask = 128;
//unsigned long codecount = 0, textcount = 0;
unsigned char buffer[N * 2];
FILE *infile, *outfile;

void error(void)
{
    printf("Output error\n");  exit(1);
}

int getbit(int n) /* get n bits */
{
    int i, x;
    static int buf, mask = 0;
    
    x = 0;
    for (i = 0; i < n; i++) {
        if (mask == 0) {
            if ((buf = fgetc(infile)) == EOF) return EOF;
            mask = 128;
        }
        x <<= 1;
        if (buf & mask) x++;
        mask >>= 1;
    }
    return x;
}

void decode(void)
{
    int i, j, k, r, c, count, count_comma;
    count = 0;
    count_comma = 0;
    char val;
    
    for (i = 0; i < N - F; i++) buffer[i] = ' ';
    r = N - F;
    while ((c = getbit(1)) != EOF) {
        if (c) {
            if ((c = getbit(8)) == EOF) break;
            fputc(c - key, outfile);
            buffer[r++] = c;  r &= (N - 1);
        } else {
            if ((i = getbit(EI)) == EOF) break;
            if ((j = getbit(EJ)) == EOF) break;
            for (k = 0; k <= j + 1; k++) {
                c = buffer[(i + k) & (N - 1)];
                fputc(c - key, outfile);
                buffer[r++] = c;  r &= (N - 1);
            }
        }
        //printf("%d",count_comma);
    }
}

int main(int argc, char *argv[]){    
    clock_t t;
    infile = fopen("EncryptedCompressed.csv", "rb");
    outfile = fopen("OutputUnfiltered.csv", "wb");
    t = clock();
    decode();
    t = clock() - t;
    //fputc('\n', outfile);
    fclose(infile);
    fclose(outfile);

    double time_taken = ((double)t)/CLOCKS_PER_SEC;
    //printf("Decompression time: %f\n",time_taken);
    return 0;
}