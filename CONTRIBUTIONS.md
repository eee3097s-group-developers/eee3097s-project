# Contributions

We welcome all contributions to further develop and improve the system design laid out in this repository. These contributions can be any of the following:  
- Proposing an improvement or new feature
- Reporting an issue or bug
- Submitting a solution or fix. 
 
## How to Contribute?
To contribute in any manner, a pull request (PR) must be made on gitlab and a member of the existing team will evaluate your request and grant you access to the repo. You may then clone the repo and create a branch whereby you can edit, delete or add to the project and its documentation and code. When contributing to this project please do the following:
update the READ.ME to document any changes or additions you have made so that the documentation is up to date.
This branch will be evaluated by a member of the developmental team before being committed.

## Licence
All contributions must adhere to the Creative Commons Legal Code licence that covers the rest of the project.  
A link to this licence can be found at:  
https://gitlab.com/eee3097s-group-developers/eee3097s-project/-/blob/main/LICENSE.md 

## Software
For ease of collaboration, please try to utilise the software and development tools used in the original creation of this project.
- STM32CubeIDE is used as the IDE to code and configure the STM32F051R8T6 Discovery boards and its pins.

However, if other software offers additional features that have the ability to enhance the project, please contact a member of the project team to discuss its potential use.

## Code of Conduct
This platform seeks to welcome and foster any contribution made towards the improvement of this project. This environment aims to be all inclusive and harassment free - regardless of age, race, gender, level of experience, nationality or religious beliefs. Any instances of abuse, harassment or behaviour that is not aligned with the outlined code of conduct will be met with the immediate removal of the offender and future prohibition of his/her participation in the project. 

This aligns with gitlab’s code of conduct:  
https://about.gitlab.com/community/contribute/code-of-conduct/

## References
This contribution guideline has been adapted from two templates referenced by the following links:  
https://gist.github.com/PurpleBooth/b24679402957c63ec426  
https://github.com/auth0/open-source-template/blob/master/GENERAL-CONTRIBUTING.md


