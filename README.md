# EEE3097S Project

<div align="center">
<img src="/Images/UCT_Logo.jpg" width=300 />
</div>

### University of Cape Town
Course Supervisor: Professor Amit Mishra
## Authors
Jason Peyron
<br />Ryan Silverman

## Project Description
This GitLab page contains all the necessary files to implement a Compression and Encryption system on an STM32F0 microcontroller.
Data is directly obtained from an IMU (Inertial Measurement Unit). The chosen IMU for implementation is the Sparkfun ICM-20948 (Qwiic) device. 
This project utilises the LZSS compression algorithm and makes use of an ascii-shifting encryption algorithm.

The LZSS source file can be located in the "Compression" folder - this includes the compress.c and decompress.c files. Additionally, the data used and outputted from the compression block during the implementation testing phase can be found in a subfolder labelled "Data".

Similarly, the encryption algorithm can be located in the "Encryption" folder - this includes encrypt.c and decrypt.c. The data used and outputted from the encryption block during the implementation testing phase is, likewise, found in a subfolder named "Data".

The combination of these algorithms can be located in the "Implementation" folder whereby the accompanying python script is found to assist with the retrieval of the IMU data from the STM32F0 MCU. The "decompressDecrypt.c" file was used in the final implementation of the system. The STM32F0 source code can be found in the STM32CubeIDE/EEE3097SProject folder which will need to be downloaded and imported into the STM32F0CubeIDE workspace, and then flashed to your STM32F0 microcontroller.

# Set Up

<div align="center">
<img src="/Images/System_Design.jpg" width=250 />
</div>

## Connect the USB-to-UART Converter to the STM32F0 Discovery Board
RXD (UART) to PA2 (STM32)
<br />GND (UART) to GND (STM32)
<br />5V (UART) to 5V (STM32)

<br />Power UART by plugging it into the PC. Can plug STM32 to PC in order to flash the code via the ST-LINK debugger.

## Connect the ICM-20948 to the STM32F0
Depending on what communication protocol the user wishes to use (namely I2C or SPI), will dictate what pins will need to be connected.
<br />Further documentation can be referenced for pin connections at:
<br />https://www.st.com/resource/en/user_manual/um1525-stm32f0discovery-discovery-kit-for-stm32-f0-microcontrollers-stmicroelectronics.pdf
<br />https://www.sparkfun.com/products/15335


## Run program
In order to run the program, the files inside the "Implementation" folder need to be downloaded - this includes "decompressDecrypt.c", "filter.c" and "serialReceive.py". The decompressDecrypt.c and filter.c programs must be compiled before running the serialReceive.py program which can be done as following:
<br />gcc decompressDecrypt.c -o decompressDecrypt.exe
<br />gcc filter.c -o filter.exe

Now run the STM32CubeIDE program and subsequently run the serialReceive.py. The outputted IMU data after being decompressed and decrypted will appear in a file called "OutputFiltered.csv".
